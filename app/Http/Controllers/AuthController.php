<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Log;
use App\Models\Profile;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['userName', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = User::all()
                    ->where('userName',$credentials['userName'])
                    ->where('enabled','1')
                    ->first();
        if($user == null)
            return response()->json(['error' => 'Unauthorized'], 401);
        //$user = User::where('userName',$credentials['userName'])->get()->first();
        $profile = Profile::join('users','users.userId','=','profiles.userId')
                            ->join('model_has_roles','model_has_roles.model_id','users.userId')
                            ->join('roles','roles.id','model_has_roles.role_id')
                            ->where('userName',$credentials['userName'])
                            ->select('profiles.*','roles.name as role')
                            ->get()
                            ->first();
        return response()->json([
            'success' => true,
            'token' => $token,
            //'user' => $user,
            'profile' => $profile
        ], 200);
        //return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $permission = $request->permission;
        if(!auth()->user())
            return response()->json(['error' => 'Unauthorized'], 401);
        if(!auth()->user()->can($permission))
            return response()->json(['error' => 'Unauthorized'], 400);
        auth()->user()->privileges = auth()->user()->getAllPermissions();
        return response()->json(auth()->user());
    }

    public static function getUser()
    {
        if(auth()->user())
            return auth()->user();
        return "NOT";
    }

    public static function prueba($token){
        return auth()->setToken($token)->user();
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public static function pruebalogout($token)
    {
        auth()->logout();
        auth()->setToken($token)->logout();

        //return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
