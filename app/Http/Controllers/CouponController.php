<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Http\Controllers\CodeController;

class CouponController extends Controller
{
    public static function updateJson(){
        $couponsJson = Coupon::join('codes','codes.codeId','=','coupons.codeId')
                    ->select('referenceId','productCode','productDescription','coupons.*')
                    ->where('codes.enabled','1')
                    ->orderBy('codes.codeId','desc')
                    ->get()
                    ->toJson();
        try{
            $file = fopen("app-assets/data/coupons-list.json", "w+b");
            fwrite($file, $couponsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function findById(Request $request){
        try{
            $coupon = Coupon::join('codes','codes.codeId','=','coupons.codeId')
                    ->select('referenceId','productCode','recommendedRetailPrice','productDescription','currency','coupons.*')
                    ->where('coupons.couponId',$request->couponId)
                    ->first();
            return response()->json([
                'success' => true,
                'coupon' => $coupon,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public static function createCoupons($codeId,$coupons){
        try{
            foreach($coupons as $coupon){
                $expiryDate = $coupon['expiryDate'];
                $codeId = $codeId;
                $serials = $coupon['serials'];
                $pins = $coupon['pins'];
                $serial = "";
                $pin = "";
                for($i=0;$i<count($serials);$i++){
                    if($i>0)
                        $serial.="¡-!";
                    $serial.=$serials[$i];
                }
                for($i=0;$i<count($pins);$i++){
                    if($i>0)
                        $pin.="¡-!";
                    $pin.=$pins[$i];
                }
                $actualCoupon = new Coupon([
                    'expiryDate' => $coupon['expiryDate'],
                    'serial' => $serial,
                    'pin' => $pin,
                    'codeId' => $codeId
                ]);
                $actualCoupon->save();
            }
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function update(Request $request){
        try{
            $coupon = Coupon::all()->where('couponId',$request->couponId)->first();
            $coupon->status = '0';
            $coupon->save();
            CodeController::decreaseQuantity($coupon->codeId);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function read(){
        self::updateJson();
    }

}
