<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public static function updateJson(){
        $usersJson = User::join('profiles','users.userId','=','profiles.userId')
                        ->select('profiles.*','users.userName')
                        ->where('users.enabled','1')
                        ->get()
                        ->toJson();
        try{

            $file = fopen("app-assets/data/users-list.json", "w+b");
            fwrite($file, $usersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function findByUserName(Request $request){
        $userName = $request->userName;
        $user = User::where('userName',$userName)->first();
        if($user != null)
            return response()->json(['user' => $user], 200);
        else
            return response()->json('Not Found',500);
    }

    public function createUserProfile($request){
        $profile = new Profile($request->all());
        $insertedUser = User::all()->last();
        $profile->userId = $insertedUser->userId;
        $profile->save();
    }

    public function create(Request $request){
        try{
            $user = new User($request->all());
            $user->password = Hash::make($request->password);
            $user->save();
            $role = Role::find(2);
            $user->assignRole($role->name);
            self::createUserProfile($request);
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        self::updateJson();
        return response()->json(['response' => 'success'], 200);
    }

    public function update(Request $request){
        try{
            $userId = $request->userId;
            $user = User::all()->where('userId',$userId)->first();
            $user->userName = $request->userName;
            $user->password = $request->password;
            $user->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $userId = $request->userId;
            $user = User::all()->where("userId",$userId)->first();
            $user->enabled = "0";
            $user->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('userName', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('user'));
    }

    public function quantity(){
        $count = User::select('*')
                        ->where('enabled','1')
                        ->get()
                        ->count();

        return $count;
    }
}
