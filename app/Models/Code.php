<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    use HasFactory;

    protected $fillable = [
        'codeId',
        'referenceId',
        'paymentId',
        'productCode',
        'quantity',
        'deliveryQuantity',
        'currency',
        'unitPrice',
        'totalPrice',
        'merchantProductCode',
        'purchaseStatusCode',
        'purchaseStatusDate',
        'productDescription',
        'totalPayablePrice',
        'taxAmount',
        'recommendedRetailPrice',
        'isReversible',
        'version',
        'signature',
        'applicationCode',
        'status',
        'userId',
        'enabled'
    ];

    protected $primaryKey = 'codeId';
}
