<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('codes', function (Blueprint $table) {
            $table->increments('codeId');
            $table->string('applicationCode',32)->default('201505190434');
            $table->string('version',5)->default('v1');
            $table->string('referenceId',50)->default('nifu-1');
            $table->string('productCode',15);
            $table->integer('quantity')->default(1);
            $table->string('merchantProductCode',20)->nullable()->default("");
            $table->string('consumerEmail',100)->nullable()->default("");
            $table->string('consumerIP',20)->nullable()->default("");
            $table->string('consumerCountryCode',2)->nullable()->default("");
            $table->string('customValue',200)->nullable()->default("");
            $table->string('signature',50);
            $table->string('enabled','1')->default('1');
            $table->timestamps();
            $table->unsignedInteger('userId');
            $table->foreign('userId')->references('userId')->on('users');
        });*/
/*zeballos.jhamil@usfx.bo
apellido paterno materno nombres-PerfilProyecto-79301442.pdf
ZeballosJhamil-PerfilProyecto-70322119.PDF asunto SHC180 Perfil de Proyecto
preparar diapositivas con un máximo de 10 diapos{
    caratula con nombre de usfx mi nombre facu
    situación problemática 2mins
    arbol de problemas
    problema
    abordaje del problema
    objetivo general "textual"
    objetivos específicos "textual"
    justificación
    metodología cronograma
    añadir por si acaso marco teórico
    añadir por si acaso marco conceptual
    dura 10 mins
}
*/
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('codeId');
            $table->string('referenceId',50)->default("");
            $table->string('paymentId',50)->default("");
            $table->string('productCode',15);
            $table->integer('quantity');
            $table->integer('deliveredQuantity')->nullable();
            $table->string('currency',10)->nullable();
            $table->text('coupons')->nullable();
            $table->float('unitPrice')->nullable();
            $table->float('totalPrice')->nullable();
            $table->string('merchantProductCode',20)->nullable()->default("");
            $table->string('purchaseStatusCode',2)->nullable();
            $table->string('purchaseStatusDate',50)->nullable();
            $table->string('productDescription',200)->nullable();
            $table->float('totalPayablePrice')->nullable();
            $table->float('taxAmount')->nullable();
            $table->float('recommendedRetailPrice')->nullable();
            $table->string('isReversible','6')->nullable();
            $table->string('version',5)->default('v1');
            $table->string('signature',50)->nullable();
            $table->string('applicationCode',32)->nullable();
            $table->string('status','1')->default('0');
            $table->string('enabled','1')->default('1');
            $table->timestamps();
            $table->unsignedInteger('userId');
            $table->foreign('userId')->references('userId')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
