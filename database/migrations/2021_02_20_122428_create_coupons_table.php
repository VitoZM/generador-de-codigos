<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('couponId');
            $table->date('expiryDate');
            $table->string('serial',100);
            $table->string('pin',100);
            $table->string('status',1)->default('1');
            $table->timestamps();
            $table->unsignedInteger('codeId');
            $table->foreign('codeId')->references('codeId')->on('codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
