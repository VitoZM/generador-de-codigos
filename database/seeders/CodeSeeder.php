<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('codes')->insert([
            'codeId' => 21,
            'referenceId' => 'nif-21',
            'paymentId' => 'MDO56916',
            'productCode' => '0247141',
            'quantity' => 2,
            'deliveredQuantity' => 2,
            'currency' => 'ARS',
            'unitPrice' => 500.00,
            'totalPrice' => 1000.00,
            'coupons' => '[{"expiryDate":"2023-11-17","serials":["771006505783101232"],"pins":["99997710065051232"]},{"expiryDate":"2023-11-17","serials":["771006505783101233"],"pins":["99997710065051233"]},{"expiryDate":"2023-11-17","serials":["771006505783101234"],"pins":["99997710065051234"]}]',
            'merchantProductCode' => '',
            'purchaseStatusCode' => '00',
            'purchaseStatusDate' => '2021-02-08T18:22:55Z',
            'productDescription' => 'Código Steam ARS 500',
            'totalPayablePrice' => 970.00,
            'taxAmount' => 0.00,
            'recommendedRetailPrice' => 500.00,
            'isReversible' => true,
            'version' => 'v1',
            'signature' => '6dc98a5bfef02f31301c3c4012e8bbe5',
            'applicationCode' => '202102030470',
            'status' => '1',
            'userId' => 1,
            'created_at' => '2021/02/08 08:00:00',
            'updated_at' => '2021/02/08 08:00:00'
        ]);
    }
}
