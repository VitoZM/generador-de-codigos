<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coupons')->insert([
            'expiryDate' => '2023-11-17',
            'serial' => '771006505783101153',
            'pin' => '99997710065051153',
            'codeId' => 21
        ]);

        DB::table('coupons')->insert([
            'expiryDate' => '2023-11-17',
            'serial' => '771006505783101154',
            'pin' => '99997710065051154',
            'codeId' => 21
        ]);
    }
}
