<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'user', 'guard_name' => 'web']);

        Permission::create(['name' => 'users-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-users', 'guard_name' => 'web']);

        Permission::create(['name' => 'codes-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-codes', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-codes', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-codes', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-codes', 'guard_name' => 'web']);

        Permission::create(['name' => 'create-coupons', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-coupons', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-coupons', 'guard_name' => 'web']);

        Permission::create(['name' => 'read-logs', 'guard_name' => 'web']);

    }
}
