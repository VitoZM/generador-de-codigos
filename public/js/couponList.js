var actualCouponId;
isLogged("read-coupons");
read();

function read() {
    let url = "api/coupon/read";
    $.post({
        url: url,
        success: (response) => {
            fillCoupons();
        },
    });
}

function fillUseModal(event,couponId){
    event.preventDefault();
    actualCouponId = couponId;
    $('#useModal').modal('toggle');
}

function update(){
    let url = "api/coupon/update";
    let data = {couponId: actualCouponId};
    $.post({
        url: url,
        data: data,
        success: (response) => {
            //console.log(response);
            read();
        },
    });
}

function showModal(event,couponId){
    event.preventDefault();
    actualCouponId = couponId;
    let url = "api/coupon/findById";
    let data = {couponId: couponId};
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                //console.log(response);
                let coupon = response.coupon;
                fillModal(coupon);
                $('#couponModal').modal('toggle');
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function fillModal(coupon){//falta llenar esto
    $('#modalCurrency').val(coupon.currency);
    $('#modalExpiryDate').val(coupon.expiryDate);
    $('#modalSerial').val(coupon.serial);
    $('#modalPin').val(coupon.pin);
    $('#modalProductCode').val(coupon.productCode);
    $('#modalProductDescription').val(coupon.productDescription);
    $('#modalRecommendedRetailPrice').val(coupon.recommendedRetailPrice);
    $('#modalReferenceId').val(coupon.referenceId);
    $('#modalStatus').val(getStringStatus(coupon.status));
    if(coupon.status == '0')
        $('#useBtn').fadeOut();
    else
        $('#useBtn').fadeIn();
}

function getStringStatus(status){
    switch(status){
        case "1":
            return "Disponible";
        case "0":
            return "Usado";
    }
}

$('#updateBtn').on('click', function(event){
    event.preventDefault();
    update();
})

$('#useBtn').on('click', function(event){
    event.preventDefault();
    update();
    $('#couponModal').modal('toggle');
})

