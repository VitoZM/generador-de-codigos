var actualUserId;

$(document).ready(function () {
    isLogged("read-users");
    fillUsers();
});

function read() {
    let url = "api/user/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {

        },
    });
}

function deleteUser(userId) {
    let url = "api/user/delete";
    let data = {
        userId: userId,
    };
    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                //console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario eliminado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillUsers();
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#deleteBtn").on("click", (event) => {
    event.preventDefault();
    deleteUser(actualUserId);
});

function cleanErrors(){
    $("#nameErrorMessage").removeClass("d-block");
    $("#nameErrorMessage").addClass("d-none");
    $("#lastNameErrorMessage").removeClass("d-block");
    $("#lastNameErrorMessage").addClass("d-none");
}

function fillEditModal(userId, userName) {
    cleanErrors();
    actualUserId = userId;
    let profile = getProfile(userId);
    $("#formUserName").val(userName);
    $("#name").val(profile.name);
    $("#lastName").val(profile.lastName);
}

function getStringMonth(number){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months[parseInt(number)];
}

$("#updateBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    update();
    $("#editarUsuario").modal("hide");
});

function error() {
    let lastNameError = validateLastName();
    let nameError = validateName();
    let passwordError = validatePassword();

    if (nameError || lastNameError || passwordError) return true;
    return false;
}

function validateName(){
    let name = $("#name").val().trim();
    if (name == "") {
        $("#nameErrorMessage").removeClass("d-none");
        $("#nameErrorMessage").addClass("d-block");
        $("#name").focus();
        return true;
    } else {
        $("#nameErrorMessage").removeClass("d-block");
        $("#nameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateLastName(){
    let name = $("#lastName").val().trim();
    if (name == "") {
        $("#lastNameErrorMessage").removeClass("d-none");
        $("#lastNameErrorMessage").addClass("d-block");
        $("#lastName").focus();
        return true;
    } else {
        $("#lastNameErrorMessage").removeClass("d-block");
        $("#lastNameErrorMessage").addClass("d-none");
        return false;
    }
}

function validatePassword() {
    let password = $("#password").val().trim();
    if (password == "") {
        $("#passwordErrorMessage").removeClass("d-none");
        $("#passwordErrorMessage").addClass("d-block");
        $("#password").focus();
        return true;
    } else {
        $("#passwordErrorMessage").removeClass("d-block");
        $("#passwordErrorMessage").addClass("d-none");
        return false;
    }
}

function update(){
    let name = $("#name").val();
    let lastName = $("#lastName").val();

    let url = "api/profile/update";
    $.post({
        url: url,
        data: {
            userId: actualUserId,
            name: name,
            lastName: lastName,
        },
        statusCode: {
            200: (response) => {

                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillUsers();
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getProfile(userId) {
    let profile;
    let url = "api/profile/getProfile";
    let data = { userId: userId };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            profile = response;
        },
    });
    return profile;
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
