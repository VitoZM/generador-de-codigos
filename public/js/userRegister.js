isLogged('create-users');

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let lastNameError = validateLastName();
    let nameError = validateName();
    let passwordError = validatePassword();
    let userNameError2 = validateUserName2();
    let userNameError = validateUserName();

    if (userNameError || userNameError2 || passwordError || nameError || lastNameError) return true;
    return false;
}

function validateName(){
    let name = $("#name").val().trim();
    if (name == "") {
        $("#nameErrorMessage").removeClass("d-none");
        $("#nameErrorMessage").addClass("d-block");
        $("#name").focus();
        return true;
    } else {
        $("#nameErrorMessage").removeClass("d-block");
        $("#nameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateLastName(){
    let name = $("#lastName").val().trim();
    if (name == "") {
        $("#lastNameErrorMessage").removeClass("d-none");
        $("#lastNameErrorMessage").addClass("d-block");
        $("#lastName").focus();
        return true;
    } else {
        $("#lastNameErrorMessage").removeClass("d-block");
        $("#lastNameErrorMessage").addClass("d-none");
        return false;
    }
}

function validatePassword() {
    let password = $("#password").val().trim();
    if (password == "") {
        $("#passwordErrorMessage").removeClass("d-none");
        $("#passwordErrorMessage").addClass("d-block");
        $("#password").focus();
        return true;
    } else {
        $("#passwordErrorMessage").removeClass("d-block");
        $("#passwordErrorMessage").addClass("d-none");
        return false;
    }
}

function validateUserName2(){
    let invalid;
    let userName = $("#formUserName").val().trim();
    let url = "api/user/findByUserName";
    $.post({
        url: url,
        data: {
            userName: userName,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#userNameErrorMessage2").removeClass("d-none");
                $("#userNameErrorMessage2").addClass("d-block");
                $("#formUserName").focus();
            },
            500: (response) => {
                invalid = false;
                $("#userNameErrorMessage2").removeClass("d-block");
                $("#userNameErrorMessage2").addClass("d-none");
            },
        }
    });
    return invalid;
}

function validateUserName() {
    let userName = $("#formUserName").val().trim();
    if (userName == "") {
        $("#userNameErrorMessage").removeClass("d-none");
        $("#userNameErrorMessage").addClass("d-block");
        $("#formUserName").focus();
        return true;
    } else {
        $("#userNameErrorMessage").removeClass("d-block");
        $("#userNameErrorMessage").addClass("d-none");
        return false;
    }
}


function create() {
    let userName = $("#formUserName").val();
    let password = $("#password").val();
    let name = $("#name").val();
    let lastName = $("#lastName").val();

    let url = "api/user/create";
    $.post({
        url: url,
        data: {
            userName: userName,
            password: password,
            name: name,
            lastName: lastName,
        },
        statusCode: {
            200: (response) => {
                //console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: "Usuario registrado Exitosamente",
                    animation: true,
                });
                clean();
            },
            500: (response) => {
               //console.log(response);
            },
        },
    });
}

function clean() {
    $("#formUserName").val("");
    $("#password").val("");
    $("#name").val("");
    $("#lastName").val("");
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
