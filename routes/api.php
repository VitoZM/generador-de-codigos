<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\CodeController;
use App\Http\Controllers\CouponController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// @TODO: Auth
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});

Route::post('/log/create', [LogController::class, 'create']);
Route::post('/log/read', [LogController::class, 'read']);
Route::post('/log/update', [LogController::class, 'update']);

Route::post('/user/create', [UserController::class, 'create']);
Route::post('/user/read', [UserController::class, 'read']);
Route::post('/user/update', [UserController::class, 'update']);
Route::post('/user/delete', [UserController::class, 'delete']);
Route::post('/user/findByUserName', [UserController::class, 'findByUserName']);

Route::post('/profile/read', [ProfileController::class, 'read']);
Route::post('/profile/update', [ProfileController::class, 'update']);
Route::post('/profile/getProfile', [ProfileController::class, 'getProfile']);

Route::post('/code/create', [CodeController::class, 'create']);
Route::post('/code/read', [CodeController::class, 'read']);
Route::post('/code/purchase', [CodeController::class, 'purchase']);
Route::post('/code/deleteActualRequest', [CodeController::class, 'deleteActualRequest']);
Route::post('/code/purchaseRequest', [CodeController::class, 'purchaseRequest']);
Route::post('/code/updatePurchasedRequest', [CodeController::class, 'updatePurchasedRequest']);
Route::post('/code/findById', [CodeController::class, 'findById']);

Route::post('/coupon/read', [CouponController::class, 'read']);
Route::post('/coupon/update', [CouponController::class, 'update']);
Route::post('/coupon/findById', [CouponController::class, 'findById']);

Route::post('/permission/read', [PermissionController::class, 'read']);
